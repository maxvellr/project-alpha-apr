from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form = form.save()
        return redirect("list_projects")

    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    task_list = Task.objects.filter(assignee=request.user)
    context = {"task_list": task_list}
    return render(request, "tasks/list.html", context)

@login_required
def delete_task(request, id):
    delete_task = Task.objects.get(id=id)
    if request.method == "POST":
        delete_task.delete()
        return redirect("show_my_tasks")

    context = {
        "delete_task":delete_task,
    }

    return render(request, "tasks/delete.html")
